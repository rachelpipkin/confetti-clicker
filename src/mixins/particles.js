import { randomizer } from './randomizer.js';

const colors = ['blue', 'green', 'yellow', 'red', 'pink'];
const shapes = ['square', 'circle', 'rectangle'];

export const particleMixin = {
  data: function() {
    return {
      settings: {
        numParticles: 50,
        distance: 50,
      },
    };
  },
  mixins: [randomizer],
  methods: {
    getContainerData(clickLocation) {
      const { settings } = this;
      const containerData = {
        x: clickLocation.x - settings.distance / 2,
        y: clickLocation.y - settings.distance / 2,
        height: settings.distance,
        width: settings.distance,
      };

      const maxY = containerData.y + containerData.height + settings.distance;
      const minY = containerData.y - settings.distance;
      const maxX = containerData.x + containerData.width + settings.distance;
      const minX = containerData.x - settings.distance;

      return { maxY, minY, maxX, minX };
    },
    getRandomParticleColor() {
      return this.getRandomArrayItem(colors);
    },
    getRandomParticleShape() {
      return this.getRandomArrayItem(shapes);
    },
  },
};
