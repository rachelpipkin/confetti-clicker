export const randomizer = {
  methods: {
    getRandomArrayItem(array) {
      return array[Math.floor(Math.random() * array.length)];
    },
    getRandomFloat(min, max) {
      return Math.random() * (max - min) + min;
    },
    getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    },
    getRotation() {
      return Math.floor(Math.random() * 360) + 1;
    },
  },
};
